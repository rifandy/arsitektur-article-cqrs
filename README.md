# ARTICLE CQRS + ES

Create Article Service, Email Service & CMS DASHBOARD

## Used create command:

CreateArticle Command 
- Payload
* `Id`
* `Title`
* `CategoryId`
* `Tags`
* `Image - M`
* `Image - D`
* `ShortDesc`

CreateContent Command 
- Payload
* `Id`
* `ArticleId`
* `Content`
* `Page`

CreateCategory Command 
- Payload
* `Id`
* `CategoryName`

CreateTags Command 
- Payload
* `Id`
* `TagName`

CreateArticleTags Command 
- Payload
* `Id`
* `TagId`
* `ArticleId`

## Used Edit command:

